#! /bin/sh
# dimanceh 13  mars 2011
# script pour lancer le système Matrix Revolutions
# pour la plateforme Linux Debian Ubuntu and alike 
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK
# on commence par tuer les instances de pd qui tournent encore
ps -e | grep pdextended 2>&1 > /dev/null
if [ $? -eq 0 ] ; then
    echo "Let's kill Puredata!"
    echo
    killall -9 pd
fi

#on envoie cash les différentes instances, sans temporisation
cd ~/gk-code;
echo "Startup directory: " $PWD
echo "Starting the main patch"
pdextended -stderr -nomidi -noaudio -open mtxrev-main.pd -send "preset-file ../presets/main.coll;" &
sleep 5s; echo "Starting the motor";
pdextended -stderr -nomidi -noaudio -open mtxrev-motor.pd -send "preset-file ./presets/motor.coll;" & 
echo "Fin du script"
